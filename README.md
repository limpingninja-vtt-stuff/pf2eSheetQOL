# PF2e Sheet QOL (Quality of Life)
Some modifications to the built in character sheet to add some QOL changes that I felt were helpful to the players.

## Attribution
While the code isn't shared, the inspiration from @Sdenec on Tidy5e was felt. Possible plans to simply create a new character sheet set later instead of js hacking the running one with a hook.

## New Features and Previews
All of the QOL features are able to be toggled by the GM with some settings available to the User.

### HP Damage Overlay
<img src='/uploads/398ce1c01fe2824a1b68616716e79866/Screenshot_2020-08-05_at_18.44.38.png' height='275px'/>

As the characters become more wounded, the image of the character will fill up to indicate the percentage of the damage. The color is configurable.

### Sheet Lock and Edit
<img src='/uploads/ff411e76ad1c9e0d39997c714190bd63/Screenshot_2020-08-05_at_18.56.25.png' height="275px"/>
<img src='/uploads/16d5f0f03a8f628c70a44f7444ca35b9/Screenshot_2020-08-05_at_18.56.44.png' height="275px"/>

The sheet can be locked or unlocked at the top level. If deletion protection is enabled then sheet will not allow deletion and hides the icons for
any of the items on the character sheet. Additionally a configuration to disable editing also exists in the settings.

QOL: When deletion occurs while the sheet is unlocked there is no more dialog, deletion in bulk can be done easily.
FIX: Later fixes will adjust the flex size of the item actions holder.

### Skills Input Restyling and Locking
<img src='/uploads/886e284301025998d75333d0305077a7/Screenshot_2020-08-05_at_21.48.28.png' width="550px"/>
<img src='/uploads/4b5b24d95dcb185cd9699704fc9894e3/Screenshot_2020-08-05_at_21.48.08.png' width="550px"/>

Skills inputs are hidden when the sheet is locked and this option is enabled.

## Installation

### Settings
