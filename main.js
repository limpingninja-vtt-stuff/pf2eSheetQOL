/**
 * This replaces the built in PF2e delete handler, the code is identical
 * but it no longer pops up a dialog to question and insted uses the toggle
 * 'allow-delete' saved in the actor flags.
 *
 * @param {*} ev
 */
let itemDeleteHandler = async ev => {
  let actor = game.actors.get(ev.data.data.actor._id);
  let toggle = actor.getFlag('pf2eSheetQOL', 'allow-delete');

  const li = $(ev.currentTarget).parents('.item');
  const itemId = li.attr('data-item-id');

  if (toggle == true) {
    await actor.deleteOwnedItem(itemId);
    // clean up any individually targeted modifiers to attack and damage
    await actor.update({
      [`data.customModifiers.-=${itemId}-attack`]: null,
      [`data.customModifiers.-=${itemId}-damage`]: null,
    });
  }
};

/**
 * Handles modification of the sheet based on the event to lock or unlock the
 * sheet. TODO: Refactor some of this. (as if everything doesn't need it)
 *
 * @param {*} event
 * @returns
 */
let sheetLockHandler = async event => {
  let actor = game.actors.get(event.data.data.actor._id);
  if (event.type === 'click') {
    if (
      $(event.currentTarget).find('#sheet-toggle-lock.fa-unlock').length == 0
    )
      return;
    $(event.currentTarget)
      .find('#sheet-toggle-lock')
      .removeClass('fa-unlock')
      .addClass('fa-lock');
    $(event.currentTarget).addClass('active');
    $(event.data.html).find('i.fas.fa-trash').hide();
    $(event.data.html).find('.skill-name input').map((idx, elem) => {
      elem.setAttribute('class', 'lock-protected');
      elem.readOnly = true;
    });
    await actor.unsetFlag('pf2eSheetQOL', 'allow-delete');
  } else if (event.type === 'contextmenu') {
    if ($(event.currentTarget).find('#sheet-toggle-lock.fa-lock').length == 0)
      return;
    $(event.currentTarget)
      .find('#sheet-toggle-lock')
      .removeClass('fa-lock')
      .addClass('fa-unlock');
    $(event.currentTarget).removeClass('active');
    $(event.data.html).find('i.fas.fa-trash').show();
    $(event.data.html).find('.skill-name input').map((idx, elem) => {
      elem.removeAttribute('class');
      elem.readOnly = false;
    });
    await actor.setFlag('pf2eSheetQOL', 'allow-delete', true);
  }
};

/**
 * Injects the lock icon into the sheet, currently in the header, but this
 * may change because it looks a bit wonky.
 *
 * @param {*} html
 * @param {*} actor
 */
let injectSheetLock = (html, actor) => {
  let lock =
    '<a class="sheet-toggle-protection active" style="float:right" title="Toggle sheet protection"><i id="sheet-toggle-lock" class="fas fa-unlock" style="font-size: 15px"></i></a>';
  let toggle = $(html).find('a.sheet-toggle-protection');
  let protectEdit = game.settings.get(
    'pf2eSheetQOL',
    'preventLockedItemEdits'
  );
  if (toggle.length == 0) {
    if (actor.getFlag('pf2eSheetQOL', 'allow-delete') == true) {
      $(html).find('div.char-details > div').before(lock);
    } else {
      $(html)
        .find('div.char-details > div')
        .before(lock.replace('fa-unlock', 'fa-lock').replace(" active", ""));
      $(html).find('i.fas.fa-trash').hide();
      if (protectEdit) $(html).find('i.fas.fa-edit').hide();
    }
  }
};

/**
 * Borrowed the overlay concept from Tidy5e, changed the calculation a bit to
 * be a little clearer. This is awkward because we don't control the sheet, it
 * injects a div after the player-image in the frame, then uses the player-image
 * attributes to set it's own height.
 *
 * @param {*} html
 * @param {*} actor
 */
let injectHPOverlay = (html, actor) => {
  let maxHp =
    actor.data.data.attributes.hp.max +
    (actor.data.data.attributes.hp.tempmax
      ? actor.data.data.attributes.hp.tempmax
      : 0);
  let actualHp =
    actor.data.data.attributes.hp.value +
    (actor.data.data.attributes.hp.temp
      ? actor.data.data.attributes.hp.temp
      : 0);

  let portrait = $(html).find('.character-details img.player-image');
  let overlay = `<div class="hp-overlay" style="max-height: ${portrait.height()}px !important; height: ${portrait.height()}px !important; background: linear-gradient(0deg, rgba(145,0,0,1) 0%, rgba(145,0,0,0) calc(115% - (${actualHp}/${maxHp})*115%), rgba(255,255,255,0) calc(115% - (${actualHp}/${maxHp})*115%), rgba(255,255,255,0) 100%);"></div>`;

  portrait.after(overlay);
  $(html).find('.character-details div.hp-overlay').on('click', function () {
    $(html).find('.character-details img.player-image').click();
  });
};

/**
 * Toggles the display of the actions on the character sheet based on the users
 * configuraiton settings.
 * 
 * @param {*} html
 * @param {*} actor
 */
let toggleDefaultActions = (html, actor) => {
  let genericList = [
    'Dodge',
    'Ready',
    'Take Cover',
    'Drop Prone',
    'Crawl',
    'Stand',
    'Seek',
    'Point Out',
    'Escape',
    'Interact',
  ];
  let actionList = $(html).find('.actions-pane ol li');
  let display = actionList.length > 0 && actionList[0].style.display == 'none'
    ? ''
    : 'none';
  actionList.map((idx, ele) => {
    if (genericList.includes(ele.innerText.trim())) {
      ele.style.display = display;
    }
  });
};


/**
 * Toggles the visibility and the editability of the input fields on any custom
 * entered skill data.
 *
 * @param {*} html
 */
let toggleSkillInput = html => {
  let toggle = $(html).find('a.sheet-toggle-protection');
  if (toggle.length == 0) {
    $(html).find('.skill-name input').map((idx, elem) => {
      elem.removeAttribute('class');
      elem.readOnly = false;
    });
  } else {
    $(html).find('.skill-name input').map((idx, elem) => {
      elem.setAttribute('class', 'lock-protected');
      elem.readOnly = true;
    });
  }
};


/**
 * Activate all of the sheet listeners based on the configuration settings for
 * the module and the user. Handles all of the calls to inject specific elements
 * and overrides some of the click actions.
 *
 * @param {*} app
 * @param {*} html
 * @param {*} data
 */
let activateListeners = (app, html, data) => {
  console.log('pf2eSheetQOL | Event replacement');
  let actor = game.actors.get(data.actor._id);

  // HP Overlay
  injectHPOverlay(html, actor);
  // Inject lock into sheet and bind actions
  if (game.settings.get('pf2eSheetQOL', 'replaceDeleteProtection')) {
    injectSheetLock(html, actor);
    $(html)
      .find('.sheet-toggle-protection')
      .click({ app: app, html: html, data: data }, sheetLockHandler.bind(html));
    $(html)
      .find('.sheet-toggle-protection')
      .contextmenu(
        { app: app, html: html, data: data },
        sheetLockHandler.bind(html)
      );

    // Override default deletion handler
    $(html).find('.item-delete').off('click');
    $(html)
      .find('.item-delete')
      .click({ app: app, data: data }, itemDeleteHandler);

    // Check skill input protection
    if (game.settings.get('pf2eSheetQOL', 'protectSkillInputs')) {
      toggleSkillInput(html);
    }
  }

  // Inject and toggle advanced actions display

  //$(".tab.actions .item.action-header .item-controls")
  // toggleDefaultActions(html, actor);

  // Find and move certain actions

  // Hide containers
  html.find('.hide-container-toggle').click(ev => {
    $(ev.target).parent().siblings().toggle(100, () => { });
  });
};

Hooks.once('ready', () => {
  console.log('pf2eSheetQOL | Init');
  //    $("head").append(`<link href="https://fonts.googleapis.com/css?family=Gloria+Hallelujah|Noto+Sans:400,700|Open+Sans:400,700|Roboto+Condensed:400,700|Roboto:400,500,700&display=swap" rel="stylesheet">`);
  game.settings.register('pf2eSheetQOL', 'replaceDeleteProtection', {
    name: 'PC Sheets: Replace delete protection',
    hint: 'This modifies and replaces the standard dialog delete protection with a single lock icon.',
    scope: 'world',
    config: true,
    default: true,
    type: Boolean,
  });

  game.settings.register('pf2eSheetQOL', 'preventLockedItemEdits', {
    name: 'PC Sheets: Prevent item edit when locked',
    hint: 'This hides the item edit icon when the sheet is locked. (Requires: Replace delete protection)',
    scope: 'world',
    config: true,
    default: true,
    type: Boolean,
  });

  game.settings.register('pf2eSheetQOL', 'protectSkillInputs', {
    name: 'PC Sheets: Reskin and lock skill inputs',
    hint: 'This will prevent editing and reskin input fields on the skills when the sheet is locked. (Requires: Replace delete protection)',
    scope: 'world',
    config: true,
    default: true,
    type: Boolean,
  });

  game.settings.register('pf2eSheetQOL', 'hpDamageOverlay', {
    name: 'PC Sheets: Enable Damage Overlay',
    hint: 'This creates a damage overlay over your character portrait that fills up red as you take damage.',
    scope: 'world',
    config: true,
    default: true,
    type: Boolean,
  });

  /*
    game.settings.register("pf2eSheetQOL", "reduceActions", {
      name: "PC Sheets: Reskin and lock skill inputs",
      hint: "This will prevent editing and reskin input fields on the skills when the sheet is locked. (Requires: Replace delete protection)",
      scope: "world",
      config: true,
      default: true,
      type: Boolean
    });
    */

  /*
    game.settings.register("pf2eSheetQOL", "gameHpOverlay", {
      name: "PC Sheets: Enables a hit point overlay.",
      hint: "Video game, like doom, style Hit Point overlay on your character's portrait.",
      scope: "user",
      config: true,
      default: false,
      type: Boolean
    });*/
});

Hooks.on('renderCRBStyleCharacterActorSheetPF2E', activateListeners);
